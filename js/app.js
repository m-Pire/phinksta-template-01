$(document).foundation();

// resize on load
jQuery(document).ready(function($) {

    var heightofnav = $('.top-bar').height();
    if ($( window ).width() > 1024) {
        $('.top-bar').css( "padding-left", $('.top-bar').outerWidth()/3 );
    }
    // resize when user changed window size
    $( window ).resize(function() {
    	if ($( window ).width() > 1024) {
            var imageCssDifference = ($(".logo").get(0).naturalHeight - $('.logo').height());
            $('.top-bar').css( "padding-left", $('.top-bar').outerWidth()/3);
    	} else {
            $('.top-bar').css( "padding-left", "1.5rem" );
    	}
    });

});

// Don't execute if we're in the Live Editor
if( !window.isCMS ) {
    // Group images by gallery using `data-fancybox-group` attributes
    var galleryId = 1;
    $('.editable-gallery').each( function() {
        $(this).find('a').attr('data-fancybox-group', 'gallery-' + galleryId++);
    });
    // Initialize Fancybox
    $('.editable-gallery a').fancybox({
        // Use the `alt` attribute for captions per http://fancyapps.com/fancybox/#useful
        beforeShow: function() {
            var alt = this.element.find('img').attr('alt');
            this.inner.find('img').attr('alt', alt);
            this.title = alt;
        }
    });
}