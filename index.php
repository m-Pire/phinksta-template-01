<?php include "header.php"; ?>



	<div class="row">
		<div class="columns large-6">
            <div class="row columns">
            <h1>FancyBox3</h1>
            <a href="https://placehold.it/1200x600/999?text=Slide-1" data-fancybox="images" data-caption="My caption">
                <img class="orbit-image" src="https://placehold.it/1200x600/999?text=Slide-1" alt="Space">
            </a>
            </div>
		    <h1>Phinksta</h1>
		    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
		</div>
		<div class="columns large-6">
            <h1>Banner Slider</h1>
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
              <div class="orbit-wrapper">
                <div class="orbit-controls">
                  <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
                  <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
                </div>
                <ul class="orbit-container">
                  <li class="is-active orbit-slide">
                    <figure class="orbit-figure">
                      <img class="orbit-image" src="https://placehold.it/1200x600/999?text=Slide-1" alt="Space">
                      <figcaption class="orbit-caption">Space, the final frontier.</figcaption>
                    </figure>
                  </li>
                  <li class="orbit-slide">
                    <figure class="orbit-figure">
                      <img class="orbit-image" src="https://placehold.it/1200x600/888?text=Slide-2" alt="Space">
                      <figcaption class="orbit-caption">Lets Rocket!</figcaption>
                    </figure>
                  </li>
                  <li class="orbit-slide">
                    <figure class="orbit-figure">
                      <img class="orbit-image" src="https://placehold.it/1200x600/777?text=Slide-3" alt="Space">
                      <figcaption class="orbit-caption">Encapsulating</figcaption>
                    </figure>
                  </li>
                  <li class="orbit-slide">
                    <figure class="orbit-figure">
                      <img class="orbit-image" src="https://placehold.it/1200x600/666&text=Slide-4" alt="Space">
                      <figcaption class="orbit-caption">Outta This World</figcaption>
                    </figure>
                  </li>
                </ul>
              </div>
              <nav class="orbit-bullets">
                <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
                <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
                <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
                <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
              </nav>
            </div>
		    <h1>Zwei zeilen Überschrift</h1>
		    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   

			Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
		</div>
	</div>

<?php include "footer.php"; ?>