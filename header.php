<!doctype html>
<html class="no-js" lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/error_compiled.css" type="text/css" rel="stylesheet">
    <link href="css/jquery.fancybox.min.css" type="text/css" rel="stylesheet">
	  <link href="s.php/app.scss" type="text/css" rel="stylesheet">
    <!-- <link href="css/app.css" type="text/css" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Phinksta Template 001</title>
  </head>
  <body>

<div class="column row show-for-small-only menu-off-canvas">
  <button type="button" class="button  menu-icon" data-toggle="offCanvasLeft1"></button>
</div>


  <div class="off-canvas-wrapper">
    <div class="off-canvas position-left" id="offCanvasLeft1" data-off-canvas>
      <!-- Your menu or Off-canvas content goes here -->

          <ul class="dropdown menu vertical" data-dropdown-menu>
            <p><img class="logo" src="img/design/logo.png"></p>
            <li>
              <a style="background-color: rgba(255, 255, 255, 0.2)" href="#"><i class="material-icons">home</i>HOME</a>
            </li>
                <li style="padding-left:1rem"><a href="#">Produkte</a></li>
                <li style="padding-left:1rem"><a href="#">Informationen</a></li>
                <li style="padding-left:1rem"><a href="https://material.io/icons/">Icons</a></li>

            </li>
            <li><a href="#">PRODUKTE</a></li>
            <li><a href="#">MITARBEITER</a></li>
            <li><a href="#">KONTAKT</a></li>
            <li><a href="#">WIR ÜBER UNS</a></li>
            <li><a href="#"><i class="material-icons">mail</i>KONTAKT</a></li>
          </ul>
          <button class="close-button" aria-label="Close menu" type="button" data-close>
            <span aria-hidden="true">&times;</span>
          </button>
    </div>
    <div class="off-canvas-content" data-off-canvas-content>
    <!-- OFF CANVAS CONTENT -->
      <div class="columns medium-4 navcontainer hide-for-small-only">

        <div data-sticky-container>

          <div class="top-bar" data-sticky data-options="marginTop:0;">
            
            <div class="top-bar-left">
              <ul class="dropdown menu vertical" data-dropdown-menu>
                <p><img class="logo" src="img/design/logo.png"></p>
                <li>
                  <a href="#"><i class="material-icons">home</i>HOME</a>
                  <ul class="menu vertical">
                    <li><a href="#">Produkte</a></li>
                    <li><a href="#">Informationen</a></li>
                    <li><a href="https://material.io/icons/">Icons</a></li>
                  </ul>
                </li>
                <li><a href="#">PRODUKTE</a></li>
                <li><a href="#">MITARBEITER</a></li>
                <li><a href="#">KONTAKT</a></li>
                <li><a href="#">WIR ÜBER UNS</a></li>
                <li><a href="#"><i class="material-icons">mail</i>KONTAKT</a></li>
              </ul>
            </div>
          </div>
        </div>


  </div>

<div class="columns medium-8 maincontainer"> <!-- space for content -->