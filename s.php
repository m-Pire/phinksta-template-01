<?php
// require "scssphp/scss.inc.php";

// use Leafo\ScssPhp\Compiler;

// $scss = new Compiler();

// echo $scss->compile();

// $scss = new scssc();
// $scss->setImportPaths("bower_components/foundation-sites/scss/");
// $scss->setFormatter("scss_formatter_compressed");
// $server = new scss_server("scss", null, $scss);
// $server->serve();

require_once "scssphp/scss.inc.php";

use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Server;

$scss = new Compiler();
$scss->addImportPath("bower_components/foundation-sites/scss/");
$scss->addImportPath("bower_components/motion-ui/");
$scss->addImportPath("scss/");
$scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');

$server = new Server('scss', null, $scss);
$server->serve();

?>